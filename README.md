# Docs

Simple documentation with docsify.

* [Docs as HTML](https://axel-klinger.gitlab.io/docs/content/index.html)

Start docs locally with [docsify](https://docsify.js.org)

```
npm install -g docsify
```
```
docsify serve content
```
